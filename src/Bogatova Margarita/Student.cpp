﻿#include "Student.h"

Student::Student(string fio, int id)//создаем студента
{
	this->ID = id;
	this->fio = fio;
	gr = nullptr;
}
Student::~Student() 
{
	delete[] Marks;
	delete gr;
}
Student * Student::getSt()
{
	return this;
}
void Student:: PrintFio() //печатаем ФИО студента
{
	cout << fio;
}
void Student::PrintMark()
{
	for (int i = 0; i < numMark; i++)
	{
		cout << Marks[i];
	}
	cout << endl;
}
string Student::getFio()
{
	return fio;
}
int Student::getId() //возвращаем ИД
{
	return ID;
}
Group*  Student::getGr() // Возвращаем группу студента
{
	return gr;
}
void  Student::setGroup(Group *gr) //задаем группу студенту
{
	this->gr = gr;
}
void  Student::addMark(int mark) //добавляем оценку студенту
{
	if (numMark == 0)
	{
		Marks = new int[numMark + 1];
		Marks[numMark] = mark;
	}
	else
	{
		int *copy = new int[numMark + 1];
		for (int i = 0; i < numMark; i++)
		{
			copy[i] = Marks[i];
		}
		Marks = new int[numMark + 1];
		Marks = copy;
		Marks[numMark] = mark;
	}
	numMark++;
}
float  Student::getAvMark() //Считаем  среднюю оценку и возвращаем ее
{
	float ave = 0, sum = 0;
	int i;
	for (i = 0; i < numMark; i++)
	{
		sum += Marks[i];
	}
	ave = sum / numMark;
	return ave;
}
int* Student::getMark()
{
	return Marks;
}
int Student::getNumMark()
{
	return numMark;
}
