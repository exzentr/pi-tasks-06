#pragma once
#include <string>
#include <iostream>
using namespace std;
class Group;
class Student
{
private:
	int ID;
	string fio;
	int *Marks;
	Group *gr;
	int numMark;
public:
	Student(string fio, int id);
	~Student(); 
	Student* getSt();
	void setGroup(Group *gr);
	void addMark(int mark);
	void PrintFio();
	void PrintMark();
	string getFio();
	int* getMark();
	int getNumMark();
	int getId();
	float getAvMark();
	Group* getGr();
	friend class Group;
};
